
% Intervalu atmetimo metodas
% Reikalavimas funkcijai - unimodalumas (vienas minimumas intervale tarp l ir r)
clc
clear all

% format shortg - defaultinis
% format longg -ilgesni decimal places.
format longg;

epsilon = 0.0001;

%disp('Intervalo pusiau dalinimo metodas')
%[res1, iter1] = fpusiau(@ftest, 0, 10, epsilon, 0);
%printRes(res1, iter1);

%disp('Auksinio pjuvio metodas')
%[res2, iter2] = fauksinis(@ftest, 0, 10, epsilon, 0.61803, 0);
%printRes(res2, iter2);

%disp('Niutono metodas')
%[res3, iter3] = fnewton(@ftest, @ftestder, @ftestder2, 5, epsilon, 0);
%printRes(res3, iter3);

disp('Intervalo pusiau dalinimo metodas')
[res1, iter1, hist1] = fpusiau(@ftikslas, 0, 10, epsilon, [0 0 0 0], []);
disp('Žingsnių į kaire skaičius: ')
disp(iter1(1, 2))
disp('Žingsnių į vidurį skaičius: ')
disp(iter1(1, 3))
disp('Žingsnių į dešinę skaičius: ')
disp(iter1(1, 4))
printRes(res1, iter1, @ftikslas);

fplot(@ftikslas, [0, 7])
hold on
title("Intevalo pusiau dalinimo metodas")
xlabel("X Ašis")
ylabel("Y Ašis")
for k=1:iter1(1,1)
    plot(hist1(k), ftikslas(hist1(k)), 'r*')
end
plot(res1, ftikslas(res1), 'g*')
hold off
figure

disp('Auksinio pjuvio metodas')
[res2, iter2, hist2] = fauksinis(@ftikslas, 0, 10, epsilon, 0.61803, [0 0 0], []);
disp('Žingsnių į kaire skaičius: ')
disp(iter2(1, 2))
disp('Žingsnių į dešinę skaičius: ')
disp(iter2(1, 3))
printRes(res2, iter2, @ftikslas);

fplot(@ftikslas, [0, 7])
hold on
title("Auksinio pjuvio metodas")
xlabel("X Ašis")
ylabel("Y Ašis")
for k=1:iter2(1,1)
    plot(hist2(k), ftikslas(hist2(k)), 'r*')
end
plot(res2, ftikslas(res2), 'g*')
hold off
figure

disp('Niutono metodas')
[res3, iter3, hist3] = fnewton(@ftikslas, @ftikslasder, @ftikslasder2, 5, epsilon, [0], []);
printRes(res3, iter3, @ftikslas);

fplot(@ftikslas, [0, 7])
hold on
title("Niutono metodas")
xlabel("X Ašis")
ylabel("Y Ašis")
for k=1:iter3(1,1)
    plot(hist3(k), ftikslas(hist3(k)), 'r*')
end
plot(res3, ftikslas(res3), 'g*')
hold off

function printRes(res, iter, fnc)
    disp('Iteraciju skaičius:')
    disp(iter(1, 1))
    disp('Gautas rezultatas (funkcijos minimumas) (X):')
    disp(res)
    disp('Funkcijos reiksme taške X (Y):')
    disp(fnc(res))
    disp('')
    disp('------')
end

% Intervalo pusiau dalinimo metodas
% iter(1, 1) - Iteraciju skaičius
% iter(1, 2) - Žingsniu kairen skaičius.
% iter(1, 3) - Žingsniu vidurin skaičius.
% iter(1, 4) - Žingsnius dešinėn skaičius
% iter(1, 5)(x, 1) - x iteracijos riba kaireje
% iter(1, 5)(x, 2) - x iteracijos riba desineje
function [res, iter, hist] = fpusiau(fnc, l, r, epsilon, iter, hist)
    iter(1, 1) = iter(1, 1) + 1;
    
    xm = (l + r) / 2;
    hist = [hist; xm];
    
    ilgis = r - l; %destytojo skaidrese L didelis.
    if ilgis < epsilon
        res = xm;
        return
    end
    
    ilgis4 = ilgis / 4;
    x1 = l + ilgis4;
    x2 = r - ilgis4;
    
    if fnc(x1) < fnc(xm)
        iter(1, 2) = iter(1, 2) + 1;
        [res, iter, hist] = fpusiau(fnc, l, xm, epsilon, iter, hist);
    elseif fnc(x2) < fnc(xm)
        iter(1, 4) = iter(1, 4) + 1;
        [res, iter, hist] = fpusiau(fnc, xm, r, epsilon, iter, hist);
    else
        iter(1, 3) = iter(1, 3) + 1;
        [res, iter, hist] = fpusiau(fnc, x1, x2, epsilon, iter, hist);
    end
end

% Auksinio pjuvio algoritmas
% iter(1, 1) - iteraciju skaicius
% iter(1, 2) - zingsniu kairen skaicius
% iter(1, 3) - zingsniu desinen skaicius
function [res, iter, hist] = fauksinis(fnc, l, r, epsilon, gamma, iter, hist)
    iter(1, 1) = iter(1, 1) + 1;
    ilgis = r - l;
    
    xm = (r + l) / 2;
    hist = [hist; xm];
    if ilgis < epsilon
        res = (r + l) / 2;
        return
    end
    gilgis = gamma * ilgis;
    x1 = r - gilgis;
    x2 = l + gilgis;
    
    if fnc(x2) < fnc(x1)
        iter(1, 3) = iter(1, 3) + 1;
        [res, iter, hist] = fauksinis(fnc, x1, r, epsilon, gamma, iter, hist);
    else
        iter(1, 2) = iter(1, 2) + 1;
        [res, iter, hist] = fauksinis(fnc, l, x2, epsilon, gamma, iter, hist);
    end
end

% Niutono metodas
function [res, iter, hist] = fnewton(fnc, dfnc, ddfnc, guess, epsilon, iter, hist)
    iter(1, 1) = iter(1, 1) + 1;
    next = guess - (dfnc(guess) / ddfnc(guess));
    hist = [hist; next];
    
    if abs(next - guess) >= epsilon
        [res, iter, hist] = fnewton(fnc, dfnc, ddfnc, next, epsilon, iter, hist);
    else
        res = next;
    end
end

% Šita naudojau saviem bandymam.
function y = ftest(x)
    x = x - 6;
    y = x * x;
end

function y = ftestder(x)
    x = x - 6;
    y = 2 * x;
end

function y = ftestder2(x)
    y = 2;
end

% Bandom su užduoties salyga
% Mano LSP: 1910630
% čia a ir b – studento knygelės numerio “1*1**ab”
% a = 3
% b = 0 => 1 + 9 + 1 + 6 + 3 = 20 => 2 + 0 = 2 => b = 2
% diff 
function y = ftikslas(x)
    y = (((x ^ 2) - 3) ^ 2) / 1;
end

function y = ftikslasder(x)
    y = (4 * x) * ((x ^ 2) - 3);
end

function y = ftikslasder2(x)
    y = (8 * (x ^ 2)) + 4 * ((x ^ 2) - 3);
end